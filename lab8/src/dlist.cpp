/*
Clayton Tucker
Lab 8

This lab simulates how a double linked list functions
*/

#include <iostream>
#include "dlist.hpp"

//using namespace std;

Dnode* Dnode::Next()
{
    return flink;
}

Dnode* Dnode::Prev()
{
    return blink;
}

//constructor 
Dlist::Dlist()
{
    //creates a new dnode to make equal to sentinel
    Dnode *temp = new Dnode;
    temp->blink = temp;
    temp->flink = temp;
    sentinel = temp;

    size = 0;
}

//copy constructor
Dlist::Dlist(const Dlist &d)
{
    Dnode *temp = new Dnode;
    temp->blink = temp;
    temp->flink = temp;
    sentinel = temp;

    size = 0;
    *this = d;
}

//assignment overload operator
Dlist& Dlist::operator= (const Dlist &d)
{
    Dnode *temp;

    Clear();
    for (temp = d.Begin(); temp != d.End(); temp = temp->Next()) Push_Back(temp->s);
    return *this;

}

//deconstructs everything from dlist
Dlist::~Dlist()
{
    Clear();
    delete sentinel;
}

void Dlist::Clear()             // This should not delete the sentinel node.
{
    while (sentinel->Next() != sentinel) Erase(sentinel->Next());
    return;
}

bool Dlist::Empty() const
{
    return (size ==0);
}

size_t Dlist::Size() const 
{
    return size;
}

/* Put new strings on the front or back of the list */

void Dlist::Push_Front(const std::string &s) 
{
    Insert_After(s, sentinel);
    return;
}

void Dlist::Push_Back(const std::string &s)
{
    Insert_Before(s, sentinel);
    return;
}

/* Remove and return the first or last element of the list */

//stores first element string of list. Deletes first element. Then returns the stored string
std::string Dlist::Pop_Front()
{
    if (sentinel->flink == sentinel) throw((std::string)"");            //checks if what is trying to be popped is sentinel
    std::string pop;

    pop = sentinel->flink->s;

    Erase(sentinel->flink);

    //size--;
    return pop;
}

//stores last element string of list. Deletes last element. Then returns the stored string
std::string Dlist::Pop_Back()
{
    if (sentinel->blink == sentinel) throw((std::string)"");           //checks if what is trying to be popped is sentinel
    std::string pop;
    pop =  sentinel->blink->s;
    Erase(sentinel->blink);
    //size--;                                                           decreases in erase
    return pop;
}


Dnode* Dlist::Begin() const         // Pointer to the first node on the list 
{
    return sentinel->flink;
}

Dnode* Dlist::End() const           // Pointer to "one past" the last node on the list.
{
    
    return sentinel;
}

Dnode* Dlist::Rbegin() const         // Pointer to the last node on the list (reverse begin)
{
    return sentinel->blink;
}

Dnode* Dlist::Rend() const           // Pointer to "one before" the first node on the list.
{
    return sentinel;
}

void Dlist::Insert_Before(const std::string &s, Dnode *n)
{
    Dnode *temp = new Dnode;
    Dnode *btemp = n->blink;             //stores n->blink information before it is removed
    

    btemp->flink = temp;
    temp->flink = n;

    temp->blink = btemp;
    n->blink = temp;

    temp->s = s;
    size++;
    return;
}

void Dlist::Insert_After(const std::string &s, Dnode *n)
{
    Dnode *temp = new Dnode;
    Dnode *ftemp = n->flink;             //stores n->flink information before it is removed
    
    ftemp->blink = temp;
    temp->blink = n;
    
    temp->flink = ftemp;
    n->flink = temp;

    temp->s = s;
    size++;
    return;
}

//erases a single dnode n
void Dlist::Erase(Dnode *n)
{
    if (n == sentinel) throw((std::string)"");
    Dnode *ftemp = n->flink;
    Dnode *btemp = n->blink;


    ftemp->blink = n->blink;         //links the node infront of *n's blink to the node behind n points too
    btemp->flink = n->flink;         //links the node behind of *n's flink to the node infront n points too

    delete n;
    size --;                            //decreases size of dlist
    return;
}