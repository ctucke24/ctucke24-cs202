#include "hash_202.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <cstdio>
#include <stdio.h>

using namespace std;

size_t XOR(const string &key);
size_t Last7(const string &key);
size_t Linear(size_t, size_t);
size_t Double(size_t, size_t);

string Hash_202::Set_Up(size_t table_size, const string &fxn, const string &collision) 
{
    //error checking if hash table has been created
    if (Keys.size() != 0) {                                     //may need to revise
        return "Hash table already set up";                 
    }

    //error checking table size
    if (table_size == 0) 
    {
        return "Bad table size";
    }

    //error checking the input for hash function 
    if ((fxn != "Last7") && (fxn != "XOR")) 
    {
        return "Bad hash function";
    }

    //error checking for the collison resolution variable input 
    if ((collision != "Linear") && (collision != "Double")) 
    {
            return "Bad collision resolution strategy";
    }
        
    /* 
        Creating the hash table
    */

    //resizing keys and Vals to table size
    Keys.resize(table_size);
    Vals.resize(table_size);        //may need to revise
     
    //assigning Fxn a 0 or a 1 depending on what hash function is given
    if (fxn == "Last7") 
    {
        Fxn = 1;
    }
    else 
    {
        Fxn = 0; //fxn == XOR
    }

    //assigning coll a 'L' or a 'D' depending on what collision resolution is given
    if (collision == "Linear")
    {
        Coll = 'L';
    }
    else 
    {
        Coll = 'D'; //collision == Double
    }

    Nkeys = 0;
    return "";
}


string Hash_202::Add(const string &key, const string &val) 
{
    /* Add() adds the given key/val pair to the hash table.  If successful, it should return
       an empty string.  Add should test for the following errors, in this order,
       and return the given strings:

           - Table not set up:                "Hash table not set up"
           - Empty string for the key:        "Empty key"
           - Key not composed of hex digits:  "Bad key (not all hex digits)"
           - Empty string for the val:        "Empty val"
           - The hash table is full:          "Hash table full"
           - Cannot insert key:               "Cannot insert key"
           - The key is already in the table  "Key already in the table"
     */
    
    string lastSeven;
    string newHex;
    size_t hash1 = 0;
    size_t storeAt;
    size_t hash2;
    size_t hash2Index;
    stringstream ss;
    


    //error checking if hash table has been created
    if (Keys.size() == 0) 
    {                                     //may need to revise
        return "Hash table not set up";                 
    }

    //error checking for empty key
    if (key == "") 
    {
        return "Empty key";
    }

    //error checking for valid hex digits (48-58 == 0-9) (97-102 == a-f)                            
    for (unsigned int i = 0; i < (key.length()); i++)
    {
        if ( (48 > key.at(i)) ||  ( (57 < key.at(i)) && (key.at(i) < 65) ) || 
           ((70 < key.at(i)) && (key.at(i) < 97)) || (102 < key.at(i)) )          
        {
            return "Bad key (not all hex digits)";
        }
    }

    //error checking for empty val
    if (val == "")
    {
        return "Empty val";
    }

    //error checking if hash table is full
    if (Nkeys == Keys.size())                           //may need to add a -1 to keys.size()
    {
        return "Hash table full";
    }

    
    /* 
    HASH FUNCTIONS 
    */

    //XOR Hash function (XORs each 7 char segments of key)
    if (Fxn == 0)
    {
       hash1 = XOR(key);
       
    }
    
    //Last7 hash function (takes last 7 of key)
    else
    {
        hash1 = Last7(key);
    }


    /*
    Checking for collisions
    */


    //finds were to store the new hash at
    storeAt = hash1 % Keys.size(); 
    

    //checks to see if place is empty;
    if (Keys.at(storeAt) != "") 
    {
        //linear collision resolution
        if (Coll == 'L')
        {
            for (unsigned int k = 1; k < Keys.size(); k++) //may need to change  <=
            {
                if (Keys.at(storeAt) != "")
                {
                    storeAt = (hash1 + k) % Keys.size(); 
                }
                else    
                {
                    break; //breaks out of for loop
                }
            }
        }
        
        //Double collision resolution
        else
        {
            size_t pos;
            size_t count = 1;
            pos = storeAt;
            
            if (Fxn == 0) //if main hash is XOR
            {
                hash2 = Last7(key);
            }
            else //main hash is Last7
            {
                hash2 = XOR(key);
            }

            while (true)
            {
                
                if (Keys[storeAt].empty() == false)
                {
                    hash2Index = (hash2 % Keys.size()) * count;
                    if (hash2Index == 0)
                    {
                        hash2Index = count;
                    }
                    storeAt = (hash1 + hash2Index) % Keys.size(); 
                }
                else
                {
                    break;
                }

                if (pos == storeAt) 
                {
                    return "Cannot insert key";
                }
                count++;
            }   
        }
    }
    

    //error checking to see if key was inserted
    if (Keys[storeAt] != "") 
    {
        return "Cannot insert key";
    }
    
    //error checking if key is in table already
    for (unsigned int j = 0; j < (Keys.size() - 1); j++)
    {
        if (Keys[j] ==  key)
        {
            return "Key already in the table";
        }
    }
    
    //storing values into vectors
    Keys.at(storeAt) = key;
    Vals.at(storeAt) = val;
    Nkeys++;

    return "";
}

string Hash_202::Find(const string &key) 
{
    /* Find() returns the val associated with the given key.  If the hash table has not been 
       set up yet, or if the key is not in the hash table, or if the key is not composed of
       all hex digits, it should return an empty string. 

       Find() is not const, because it sets the variable Nprobes to equal the number of
       probes that it took to find the answer. */

    size_t probe = 0;
    string lastSeven;
    size_t hash1 = 0;
    size_t storeAt = 0;
    size_t hash2 = 0;
    size_t hash2Index = 0;
    stringstream ss;
    Nprobes = 0;

    //error checking if hash table has been created
    if (Keys.size() == 0) 
    {                                     //may need to revise
        return "";                 
    }

    //error checking for valid hex digits (48-58 == 0-9) (97-102 == a-f)                            
    for (unsigned int i = 0; i < (key.length()); i++)
    {
        if ( (48 > key.at(i)) ||  ( (57 < key.at(i)) && (key.at(i) < 65) ) || 
           ((70 < key.at(i)) && (key.at(i) < 97)) || (102 < key.at(i)) )          
        {
             return "";
        }
    }

    //XOR Hash function (XORs each 7 char segments of key)
    if (Fxn == 0)
    {
       hash1 = XOR(key);
    }
    
    //Last7 hash function (takes last 7 of key)
    else
    {
        hash1 = Last7(key);
    }

    storeAt = hash1 % Keys.size();

    //checking if there is a value at stores at
    if (Keys[storeAt] == "")
    {
        return "";
    }

    //checking if there was no collison
    if (Keys[storeAt] == key)
    {
        return Vals[storeAt];
    }
    else 
    {
        //linear collision resolution
        if (Coll == 'L')
        {
            for (unsigned int k = 1; k < Keys.size(); k++) //may need to change  <=
            {
                if (Keys.at(storeAt) != key)
                {
                    Nprobes++;
                    //cout << Nprobes << " " << Vals[storeAt] << endl;
                    storeAt = (hash1 + k) % Keys.size();  //linear resolution
                }
                else    
                {
                    break; //breaks out of for loop
                }

                if (Keys[storeAt] == key)
                {
                    return Vals[storeAt];
                }
            }
        }
        //Double collision resolution
        else
        {
            size_t pos;
            size_t count = 1;
            pos = storeAt;
            
            if (Fxn == 0) //if main hash is XOR
            {
                hash2 = Last7(key);
            }
            else //main hash is Last7
            {
                hash2 = XOR(key);
            }

            while (true)
            {
                
                if (Keys[storeAt].empty() == false)
                {
                    Nprobes++;
                    //cout << Nprobes << " " << Vals[storeAt] << endl;
                    hash2Index = (hash2 % Keys.size()) * count;
                    if (hash2Index == 0)
                    {
                        hash2Index = count;
                    }
                    storeAt = (hash1 + hash2Index) % Keys.size(); 
                }
                else
                {
                    break;
                }

                if (Keys[storeAt] == key)
                {
                    return Vals[storeAt];
                }

                if (pos == storeAt) 
                {
                    return "";
                }
                count++;
            }
        }
    }
    //Nprobes = probe;
    //return Vals[storeAt];
    return "";
}


void Hash_202::Print() const 
{
    for (unsigned int i = 0; i < Keys.size(); i++)
    {
        if (Keys[i] != "")
        {
            printf("%5u %s %s\n", i, Keys[i].c_str(), Vals[i].c_str());
        }
    }
    return;
}


size_t Hash_202::Total_Probes() 
{
    /* Total_Probes() should traverse the hash table, and for every key, caculcate how many probes
       it takes find that key.  It should return the total number of probes.  It should
       return 0 if the hash table has not been set up yet.  It is not const, because it 
       uses Find() to find the number of probes for each key. */

    size_t totProb = 0;
    
    //error checking if hash table has been created
    if (Keys.size() <= 0) 
    {                                     //may need to revise
        return 0;                 
    }

    for (unsigned int i = 0; i < Keys.size(); i++)
    {
        if (Keys[i] != "")
        {
            Find(Keys[i]);
            totProb = totProb + Nprobes;
        }
     }

    return totProb;
}


size_t XOR (const string &key) 
{
    string seven;
    size_t Seven;
  
    size_t hashVal = 0;
    stringstream ss;
   
    for (unsigned int i = 0; i < key.length(); i+=7) {
    seven = key.substr(i, 7) ;
    ss.clear();
    ss.str(seven);
    ss >> hex >> Seven;
    hashVal = hashVal ^ Seven;
    }
    return hashVal;
}

size_t Last7 (const string &key) 
{
    string lastSeven;
    size_t hashVal;
    stringstream ss;

    if (key.length() > 7){
        lastSeven = key.substr(key.length() - 7);   //temp fix for 6 0s
    }
    else 
    {
        lastSeven = key;
    }             //may need to change  
    ss.str(lastSeven);
    ss >> hex >> hashVal;

    return hashVal;
}

// size_t Linear(size_t lhashVal, size_t keySize)
// {
    
//     size_t storeAt = (lhashVal + 1) % keySize;

//     return storeAt;
// }

// size_t Double(size_t dhashVal, size_t keySize)
// {
//     return dhashVal;
// }

