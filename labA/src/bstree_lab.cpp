/*
Lab 9
Clayton Tucker
Simulates a binary tree
*/

#include <vector>
#include <string>
#include <iostream>
#include <cstdio>
#include "bstree.hpp"
using namespace std;
using CS202::BSTree;
using CS202::BSTNode;

/* ---------------------------------------------- */
/* You'll write all of these as part of your lab. */

//copy function   
BSTree::BSTree(const BSTree &t)        
{
  BSTNode *temp = new BSTNode;
  //temp = new BSTNode;
  temp->left = NULL;
  temp->right = temp;
  temp->parent = NULL;
  temp->key = "---SENTINEL---";
  temp->val = NULL;
  sentinel = temp;


  
  *this = t;


}

//assignment overload operator
BSTree& BSTree::operator= (const BSTree &t) 
{
  Clear();

  vector <string> orKeys = t.Ordered_Keys();
  vector <void *> orVals = t.Ordered_Vals();
  size = t.Size();
  sentinel->right = make_balanced_tree(orKeys, orVals, 0, size);  //seting the right of sentinel to the root of the balanced tree
  sentinel->right->parent = sentinel;                             //setting the parent of the root to sentinel
  
  return *this;
}

// Distance from a node to the root.  Returns -1 if the key is not in the tree.
int BSTree::Depth(const string &key) const
{
  BSTNode *n;
  int counter;

  n = sentinel->right;
  while (1) {
    if (n == sentinel) return -1;
    if (key == n->key) break;
    if (key < n->key)
    {
      n = n->left;
    }
    else
    {
      n = n->right;
    }
  }
  
  //counts the number of parents between key and child              may need to change it to n != sentinel
  while (n->parent != sentinel)
  {
    n = n->parent;
    counter++;
  }
  return counter;
}

// Returns the depth of the node with maximum depth, plus one.          
int BSTree::Height() const
{
  int treeHeight;
  treeHeight = recursive_find_height(sentinel->right);  //pointer to root of tree
  treeHeight++;                                         //increases the tree depth by +1;
  return treeHeight;
}

// A helper for Height()
int BSTree::recursive_find_height(const BSTNode *n) const
{
  int leftHeight, rightHeight;
  
  //base case
  if (n == sentinel) return -1;

  rightHeight = recursive_find_height(n->right);  //getting furthest right node to the right side of parent
  leftHeight = recursive_find_height(n->left);    //getting the furthest left node to the left of the parent

  if (rightHeight < leftHeight)
  {
    //cout << n->key << " " << leftHeight+1 << endl;
    return (leftHeight+1);
  }
  else
  {
    //cout << n->key << " " << rightHeight+1 << endl;
    return (rightHeight+1);
  }
}

//returns a vector of ordered keys
vector <string> BSTree::Ordered_Keys() const
{
  vector <string> rv;
  make_key_vector(sentinel->right, rv);
  return rv;
}

// A helper for Ordered_Keys()
void BSTree::make_key_vector(const BSTNode *n, vector<string> &v) const
{
  if (n == sentinel) return;
  make_key_vector(n->left, v);
  v.push_back(n->key);
  make_key_vector(n->right, v);
}

// A helper for the copy constructor and assignment overload.
BSTNode *BSTree::make_balanced_tree(const vector<string> &sorted_keys, const vector<void *> &vals,
                                    size_t first_index, size_t num_indices) const
{
  //base case
  if (num_indices == 0) return sentinel;

  BSTNode *temp = new BSTNode;

  //left
  //cout << "Left: " << first_index << " " << (num_indices << endl;
  temp->left = make_balanced_tree(sorted_keys, vals, first_index, (num_indices/2));

  //right
  //cout << "Right: " << first_index << " " << num_indices << endl;
  temp->right = make_balanced_tree(sorted_keys, vals, (first_index + (num_indices/2)+1), ((num_indices-1)/2));
 
  //setting the parent nodes
  if (temp->left != sentinel)
  {
    temp->left->parent = temp;
  }
  if (temp->right != sentinel)
  {
    temp->right->parent = temp;
  }

  //setting vals for node
  temp->key = sorted_keys[first_index + (num_indices/2)];
  temp->val = vals[first_index + (num_indices/2)];


  return temp;  //returns root node
}
