#include "pgm.hpp"
#include <cstdio>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <fstream>
using namespace std;

bool Pgm::Read(const std::string &file)
{
  ifstream fin;
  string s;
  size_t i, j, r, c, v;

  fin.open(file.c_str());
  if (fin.fail()) return false;
  
  if (!(fin >> s)) return false;
  if (s != "P2") return false;
  if (!(fin >> c) || c <= 0) return false;
  if (!(fin >> r) || r <= 0) return false;
  if (!(fin >> i) || i != 255) return false;

  Pixels.resize(r);
  for (i = 0; i < r; i++) {
    Pixels[i].clear();
    for (j = 0; j < c; j++) {
      if (!(fin >> v) || v > 255) return false;
      Pixels[i].push_back(v);
    }
  }
  if (fin >> s) return false;
  fin.close();
  return true;
}


bool Pgm::Write(const std::string &file) const {
  ofstream fin;
  int pixCount = 0;

  //checks if the pixels vector is empty
  if ((Pixels.size() == 0) || (Pixels[0].size() == 0)) {
    return false;
  }

  //opening the file and error checking if file was opened properly
  fin.open(file.c_str());
    if (fin.fail()) {
      return false;
    }

  //writing header to file
  fin << "P2" << endl;
  fin << Pixels[0].size() << " " << Pixels.size() << endl;
  fin << "255" << endl;

  for (uint c = 0; c < Pixels.size(); c++) {
    for (uint r = 0; r < Pixels[0].size(); r++) {
      pixCount++;
      //every 20 pixels, a new line is created or if the last pixel is here
      if (pixCount == 20 || (((c+1) * (r+1)) == (Pixels.size() * Pixels[0].size()))) {
        pixCount = 0;
        fin << Pixels[c][r] << endl;
      }
      else {
        fin << Pixels[c][r] << " ";
      }
    }
  }

  fin.close();
  return true; //may need to change this 
}


bool Pgm::Create(size_t r, size_t c, size_t pv) {
  //error checking for valid row, col, and pixel value
  if (r <= 0) {
    return false;
  }
  if (c <= 0) {
    return false;
  }
  if ((pv == 0) || (pv > 255)) {
    return false;
  }

  //Creates a pgm in a double vector;
  Pixels.resize(r);
  for (size_t i = 0; i < r; i++) {
    Pixels[i].clear();
    for (size_t j = 0; j < c; j++) {
      
      Pixels[i].push_back(pv);
    }
  }
  
  return true; //may need to change this 
}


bool Pgm::Clockwise() {
  vector<vector<int> > tempV;
  
  //checks if the pixels vector is empty
  if ((Pixels.size() == 0) || (Pixels[0].size() == 0)) {
    return false;
  }
    
  //swapping the row num and col num in tempVec
  tempV.resize(Pixels[0].size(), vector <int> (Pixels.size()));

  //copies pixels to rotate CW
  for(uint col = 0; col < Pixels.size(); col++) { //col
    for(uint row = 0; row < Pixels[0].size(); row++) { //row
          
        tempV[row][tempV[0].size()-col-1] = Pixels[col][row];
    }
  }

  Pixels = tempV;
  return true; 
}

bool Pgm::Cclockwise() {
  vector<vector<int> > tempV;
  
  //checks if the pixels vector is empty
  if ((Pixels.size() == 0) || (Pixels[0].size() == 0)) {
    return false;
  }
    
  //swapping the row num and col num in tempVec
  tempV.resize(Pixels[0].size(), vector <int> (Pixels.size()));

  //copies pixels to rotate CCW
  for(uint col = 0; col < Pixels.size(); col++) { //col
    for(uint row = 0; row < Pixels[0].size(); row++) { //row
          
        tempV[tempV.size()-row-1][col] = Pixels[col][row];
    }
  }

  Pixels = tempV; 
  return true; 
}

bool Pgm::Pad(size_t w, size_t pv) {
  vector<vector<int> > tempV;
  
  //checks if the pixels vector is empty
  if ((Pixels.size() == 0) || (Pixels[0].size() == 0)) {
    return false;
  }


  //checks if pv is valid
  if (pv > 255) {
    return false;
  }

  tempV.resize(Pixels.size() + (2*w), vector <int> (Pixels[0].size() + (2*w) ));

  //could create a pv backdrop and then align pixels in the center
  //There is probably a more efficient way to get the border but this is the easiest
  for(uint col = 0; col < tempV.size(); col++) { //col
    for(uint row = 0; row < tempV[0].size(); row++) { //row
      tempV[col][row] = pv;
    }
   } 

  //translates Pixels right w and down w
  for(uint col = 0; col < Pixels.size(); col++) { //col
    for(uint row = 0; row < Pixels[0].size(); row++) { //row
      tempV[col+w][row+w] = Pixels[col][row];
    }
   } 

  Pixels = tempV;
  return true; //may need to change this   
}


bool Pgm::Panel(size_t r, size_t c) {
  vector<vector<int> > tempV;
  
  //checks if the pixels vector is empty
  if ((Pixels.size() == 0) || (Pixels[0].size() == 0)) {
    return false;
  }

  if ((r == 0) || (c == 0)) {
    return false;
  }

  size_t rowN = Pixels.size();
  size_t colN = Pixels[0].size();


  //creats a temp vector that is c times long and r times as big
  tempV.resize((Pixels.size() * r) , vector <int> ((Pixels[0].size() * c )));

  for (size_t i = 0; i < tempV.size(); i++) {
    for (size_t j = 0; j < tempV[0].size(); j++) {
      tempV[i][j] = Pixels[i % rowN][j % colN];
    }
  }

  
Pixels = tempV;  
return true; //may need to change this 
}


bool Pgm::Crop(size_t r, size_t c, size_t rows, size_t cols) {
  vector<vector<int> > tempV;
  
  //checks if the pixels vector is empty
  if ((Pixels.size() == 0) || (Pixels[0].size() == 0)) {
    return false;
  }

  //checking if the passed variables are valid
  if ((r > Pixels.size()) || (c > Pixels[0].size()) || ((rows + r) > Pixels.size()) || ((cols + c) > Pixels[0].size()) || (cols == 0) || (0 ==rows )) {
    return false;
  }

  tempV.resize(rows, vector <int> (cols));
 
  //cout << cols << " " << rows << endl;

  for (size_t fcol = 0; fcol < cols; fcol++) {
    for (size_t frow = 0; frow < rows; frow++) {
      tempV[frow][fcol] = Pixels[frow + r][fcol +c];
    //tempV[fcol][frow] = Pixels[fcol + c][frow + r];



    }
  }




  Pixels = tempV;
  return true; //may need to change this   
}