#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include "avltree.hpp"
using namespace std;
using CS202::AVLTree;
using CS202::AVLNode;

bool imbalance(const AVLNode *n);   //checks child nodes to look for breaking the balance rule
void rotate(AVLNode *n);            //rotates the tree if there is an imbalance
void fix_height(AVLNode *n);        //fixes the height
void fix_imbalance(AVLNode *n);
void node_quickset(AVLNode *n, AVLNode *l, AVLNode *r, AVLNode *p); // fixes the pointers 


AVLTree& AVLTree::operator= (const AVLTree &t)        
{
  Clear();
  size = t.size;
  sentinel->right = recursive_postorder_copy(t.sentinel->right);  //seting the right of sentinel to the root of the balanced tree
  sentinel->right->parent = sentinel;                             //setting the parent of the root to sentinel
  
  return *this;
}

/* You need to write this to help you with the assignment overload.
   It makes a copy of the subtree rooted by n.  That subtree is part
   of a different tree -- the copy will be part of the tree that
   is calling the method. */

AVLNode *AVLTree::recursive_postorder_copy(const AVLNode *n) const
{
  //base case
  if (n->height == 0) return sentinel;

  AVLNode *temp = new AVLNode;
  
  //left
  //cout << "Left: " << n->left << endl;
  temp->left = recursive_postorder_copy(n->left);

  //right
  //cout << "Right: " << n->right << " ";
  temp->right = recursive_postorder_copy(n->right);
  //cout << "heck ";
  //setting the parent nodes
  if (temp->left != sentinel)
  {
    temp->left->parent = temp;
  }
  if (temp->right != sentinel)
  {
    temp->right->parent = temp;
  }

   //setting vals for node
  temp->key = n->key;
  temp->val = n->val;
  temp->height = n->height;

  return temp;
}


/* I simply took Insert and Delete from their binary search tree
   implementations.  They aren't correct for AVL trees, but they are
   good starting points.  */

bool AVLTree::Insert(const string &key, void *val)
{
  AVLNode *parent;
  AVLNode *n;

  parent = sentinel;
  n = sentinel->right;

  /* Find where the key should go.  If you find the key, return false. */

  while (n != sentinel) {
    if (n->key == key) return false;
    parent = n;
    n = (key < n->key) ? n->left : n->right;
  }

  /* At this point, parent is the node that will be the parent of the new node.
     Create the new node, and hook it in. */

  n = new AVLNode;
  n->key = key;
  n->val = val;
  n->parent = parent;
  n->height = 1;
  n->left = sentinel;
  n->right = sentinel;

  /* Use the correct pointer in the parent to point to the new node. */

  if (parent == sentinel) {
    sentinel->right = n;
  } else if (key < parent->key) {
    parent->left = n;
  } else {
    parent->right = n;
  }

  /* Increment the size */
  size++;


  //getting the correct height and sorting out any imbalances
  size_t tempHeight;
  while (n->parent != sentinel)
  {
    n = n->parent;
    tempHeight = n->height;
    fix_height(n);

    if(tempHeight == n->height)
    {
      break;
    }
    if(imbalance(n))
    {
      fix_imbalance(n);
      break;
    }
  }

  return true;
}
    
bool AVLTree::Delete(const string &key)
{
  AVLNode *n, *parent, *mlc;
  string tmpkey;
  void *tmpval;

  /* Try to find the key -- if you can't return false. */

  n = sentinel->right;
  while (n != sentinel && key != n->key) {
    n = (key < n->key) ? n->left : n->right;
  }
  if (n == sentinel) return false;

  /* We go through the three cases for deletion, although it's a little
     different from the canonical explanation. */

  parent = n->parent;

  /* Case 1 - I have no left child.  Replace me with my right child.
     Note that this handles the case of having no children, too. */

  if (n->left == sentinel) {
    if (n == parent->left) {
      parent->left = n->right;
    } else {
      parent->right = n->right;
    }
    if (n->right != sentinel) n->right->parent = parent;
    delete n;
    size--;

  /* Case 2 - I have no right child.  Replace me with my left child. */

  } else if (n->right == sentinel) {
    if (n == parent->left) {
      parent->left = n->left;
    } else {
      parent->right = n->left;
    }
    n->left->parent = parent;
    delete n;
    size--;

  /* If I have two children, then find the node "before" me in the tree.
     That node will have no right child, so I can recursively delete it.
     When I'm done, I'll replace the key and val of n with the key and
     val of the deleted node.  You'll note that the recursive call 
     updates the size, so you don't have to do it here. */

  } else {
    for (mlc = n->left; mlc->right != sentinel; mlc = mlc->right) ;
    tmpkey = mlc->key;
    tmpval = mlc->val;
    Delete(tmpkey);
    n->key = tmpkey;
    n->val = tmpval;
    return true;
  }


  //fixing height and imbalances
  //size_t tempHeight;
  n = parent;
  while (n != sentinel)
  {
    //tempHeight = n->height;
    fix_height(n);
    //if(tempHeight == n->height) break;    //if the height does not change, break;

    if (imbalance(n) == true)
    {
      fix_imbalance(n);
    }
    
    n = n->parent;
  }
  return true;
}
               
/* You need to write these two.  You can lift them verbatim from your
   binary search tree lab. */

vector <string> AVLTree::Ordered_Keys() const
{
  vector <string> rv;
  make_key_vector(sentinel->right, rv);
  return rv;
}
    
void AVLTree::make_key_vector(const AVLNode *n, vector<string> &v) const
{
  if (n == sentinel) return;
  make_key_vector(n->left, v);
  v.push_back(n->key);
  make_key_vector(n->right, v);
}
     
size_t AVLTree::Height() const
{
  return sentinel->right->height;
}

bool imbalance(const AVLNode *n)   //checks child nodes to look for breaking the balance rule
{
  size_t leftH, rightH;
  leftH = n->left->height;
  rightH = n->right->height;

  //left greater than right imbal
  if (leftH > rightH)
  {
    if (leftH - rightH > 1) return true;
    return false;
  }
  if (leftH < rightH)
  {
    if (rightH - leftH > 1) return true;
    return false;
  }
  return false;
  
  // if (abs(n->left->height - n->right->height) > 1) return true;
  // //if (n->right->height - n->left->height > 1) return true;
  // else return false;
}

void fix_height(AVLNode *n)
{
  if (n == NULL) return;
  //n->height = max(n->left->height, n->right->height) +1;
  if (n->right->height > n->left->height)    //takes the right height if the right childs height is greater or the same as teh left height
  {
    n->height = n->right->height + 1;
  }
  else 
  {
    n->height = n->left->height + 1;
  }  
}

void fix_imbalance(AVLNode *n)  //calls rotate according to zig or zag
{
  //cout << "feck" << endl;
  if (n == NULL) return;
  
  AVLNode *parent;
  AVLNode *child;
  bool pleft = false;
  bool cleft = false;
  bool zigzig = false;
 
  //determining parent
  if (n->left->height > n->right->height) 
  {
    pleft = true;
    parent = n->left;
  }
  else      
  {
    parent = n->right;  
  }

  //determining child
  if (parent->left->height > parent->right->height)
  {
    child = parent->left;
    cleft = true;
  }
  else if (parent->left->height < parent->right->height) 
  {
    child = parent->right;
  }
  else if (pleft == true)     //if it is a tie, forces child to be the same direction as parent
  {
    child = parent->left;
    cleft = true;
  }
  else
  {
    child = parent->right;
  }

  if (pleft == cleft)
  {
    zigzig = true;
  }

  //calling rotate
  if (zigzig == true)     //zigzig
  {
    rotate(parent);
    // fix_height(n);
    // fix_height(parent);
    // fix_height(child);
  }
  else                    //zigzag
  {
    rotate(child);
    rotate(child);
    // fix_height(n);
    // fix_height(parent);
    // fix_height(child);
  }

}

void rotate(AVLNode *n)           //rotates the tree if there is an imbalance
{
  AVLNode *p;   //parent
  AVLNode *g;   //grandparent
  AVLNode *m;   //middle. parents other child other than N;
  AVLNode *l;   //left child of n
  AVLNode *r;   //right child of n
  //size_t tempHeight;
  //bool rotateRight = false;

  p = n->parent;
  g = p->parent;
  l = n->left;
  r = n->right;

  // //setting granparent's child
  if (g->left == p)
  {
    g->left = n;
  }
  else
  {
    g->right = n;
  }

  if (p->left == n)     //rotate right
  {
    m = p->right;
    node_quickset(n,l,p,g);
    node_quickset(p,r,m,n);
    node_quickset(r, r->left, r->right, p);
    fix_height(p);
    fix_height(n);
  }
  if (p->right == n)    //rotate left
  {
    m = p->left;
    node_quickset(n,p,r,g);
    node_quickset(p,m,l,n);
    node_quickset(l, l->left, l->right, p);
    fix_height(p);
    fix_height(n);
  }

}


void node_quickset(AVLNode *n, AVLNode *l, AVLNode *r, AVLNode *p)      //n is the main node, l is left child, r is right child, p is parent
{
  if (l != NULL) n->left = l;
  if (r != NULL) n->right = r;
  if (p != NULL) n->parent = p;
}