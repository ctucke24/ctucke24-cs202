/*
Clayton Tucker
Lab 9 
simulates a neopet minigame
*/

#include <iostream>
#include <vector>
#include <sstream>

using namespace std;

class Shifter {
  public:

    /* Read_Grid_And_Shapes() initializes the grid from argc/argv, and the
       reads from standard input to get the shapes. */

    bool Read_Grid_And_Shapes(int argc, const char **argv);

    /* Apply_Shape() applies the shape in Shapes[index] to the grid,
       starting at row r and column c.  You'll note, if you call
       Apply_Shape twice with the same arguments, you'll end up 
       with the same grid as before the two calls. */

    bool Apply_Shape(int index, int r, int c);

    /* Find_Solution() is the recursive procedure.  It tries all possible
       starting positions for Shape[index], and calls Find_Solution() 
       recursively to see if that's a correct way to use Shape[index].
       If a recursive call returns false, then it "undoes" the shape by
       calling Apply_Shape() again. */

    bool Find_Solution(int index);

    /* This prints the solution on standard output. */

    void Print_Solution() const;

    /* This checks if the grid is solved */

    bool grid_solved();

    void isSolved();

  protected:

    bool solved;
    

    /* This is the grid.  I have this be a vector of 0's and 1's, because it makes
       it easy to print the grid out. */

    vector <string> Grid;                   

    /* These are the shapes, in the order in which they appear in standard input. */

    vector < vector <string> > Shapes;     

    /* These two vectors hold the starting positions of the shapes, both as you
       are finding a solution, and when you are done finding a solution. */

    vector <int> Solution_Rows;            
    vector <int> Solution_Cols;           
};



int main (int argc, const char **argv)
{
    Shifter shift;

    if (!shift.Read_Grid_And_Shapes(argc, argv))
    {
        cerr << "Bad Grid dimensions" << endl;
        return 1;
    }

    shift.Find_Solution(0);

    shift.isSolved();


    shift.Print_Solution();

    return 0;
}

bool Shifter::Read_Grid_And_Shapes(int argc, const char **argv)
{
    string input;
    string part;
    stringstream ss;
    int shapeNum = 0;
    vector <string> empty;

    //error checking num of arguments
    if (argc == 1) return false;   

    //inserting grid information
    for (int i = 1; i < argc; i++)
    {
        Grid.push_back(argv[i]);
    }
    
    //getting shape information
    while (getline(cin, input))
    {
        if (input == "q") break;
        //cout << "cock ";
        Shapes.push_back(empty);
        //increases the size of shape by one
        ss.str(input);
        while (ss >> part)
        {
            //cout << "2";
            Shapes[shapeNum].push_back(part);
        }
        shapeNum++;
        ss.clear();
        cin.clear();
    }
    
    solved = false;

    Solution_Rows.resize(Shapes.size());
    Solution_Cols.resize(Shapes.size()); 

    return true;
}


bool Shifter::Apply_Shape(int index, int r, int c)
{
    /* Apply_Shape() applies the shape in Shapes[index] to the grid,
       starting at row r and column c.  You'll note, if you call
       Apply_Shape twice with the same arguments, you'll end up 
       with the same grid as before the two calls. */
    
    //checking if the shape will fit in the grid parameters
    if (Shapes[index].size() + (unsigned) r > Grid.size()) return false;
    if(Shapes[index][0].size() + (unsigned) c > Grid[0].size()) return false;

    for (size_t i = 0; i < Shapes[index].size(); i++) 
    {
        for (size_t j = 0; j < Shapes[index][i].size(); j++)
        {
            //checks if the value in [i][j] is 1
            if (Shapes[index][i][j] == '1')
            {
                //flips the value in grid
                if (Grid[r+i][c+j] == '1')
                {
                    Grid[r+i][c+j] = '0';
                }
                else
                {
                    Grid[r+i][c+j] = '1';
                }
            }
        }
    }
    return true;
}

bool Shifter::Find_Solution(int index)  
{
    /* Find_Solution() is the recursive procedure.  It tries all possible
       starting positions for Shape[index], and calls Find_Solution() 
       recursively to see if that's a correct way to use Shape[index].
       If a recursive call returns false, then it "undoes" the shape by
       calling Apply_Shape() again. */
       
       //checks if it is checking for an invalid index
        if ((unsigned) index == Shapes.size())
        {
            if (grid_solved() == true)
            {
                solved = true;
                return true;
            }
            else
            {
                return false;
            }
        }
    //itterates through all grid locations looking for valid places to put shape index
    for (size_t row = 0; row < Grid.size(); row++)
    {
        for (size_t col = 0; col < Grid[row].length(); col++)
        {
            //checking base case
            if (solved == true) {
                return true;
            }
            
            //Applies shape and then fills in a possible solution
            if (Apply_Shape(index, row, col) == true)
            {
                Solution_Rows[index] = row;
                Solution_Cols[index] = col;

                //checks if insert shape failed
                if (Find_Solution(index+1) == false)
                {
                    Apply_Shape(index, row, col);
                }
                else //shape was sucessfully applied
                {
                    return true;
                }
            }
        }
    }
    return false; 
}

void Shifter::Print_Solution() const
{
    if (solved == true)
    {
        for (size_t index = 0; index < Shapes.size(); index++)
        {
            for (size_t shape = 0; shape < Shapes[index].size(); shape++)
            {
                cout << Shapes[index][shape] << " ";
            }
            cout << Solution_Rows[index] << " " << Solution_Cols[index] << endl;
        }
    }
    return;
}

// checks if grid is solved
bool Shifter::grid_solved()
{
    for (size_t r = 0; r < Grid.size(); r++)
    {
        for (size_t c = 0; c < Grid[r].length(); c++)
        {
            if (Grid[r].at(c) == '0') return false;
        }
    }
    return true;
}


void Shifter::isSolved()
{
    if (grid_solved() == true) solved = true;
    else solved = false;
}