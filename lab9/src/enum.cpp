/*
Clayton Tucker
Lab 9
This program takes two numbers a number length and number of 1s and finds the number of unique variations
*/
#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

void do_enumeration(string &s, int index, int n_ones);

int main(int argc, char** argv)
{
    //checks if arguments passed were correct
    if (argc != 3) return 0;

    //getting variables
    int size = atoi(argv[1]);
    int numOnes = atoi(argv[2]);
    string s;

    s.resize(size);
    do_enumeration(s, 0, numOnes);

    return 0;
}

void do_enumeration(string &s, int index, int n_ones)   //string, total size, number of 1s
{
    //base case if index == size print s
    if ((unsigned) index == s.size()) 
    {
        if (n_ones == 0) 
        {
            cout << s << endl;
        }
        return;
    }
    
    //checks for room for remaining 1s

    if ((s.size() - (unsigned) index) > (unsigned) n_ones)
    {
        s[index] = '0';
        do_enumeration(s, index + 1, n_ones);
    }
    s[index] = '1';
    do_enumeration(s, index + 1, n_ones - 1);
}