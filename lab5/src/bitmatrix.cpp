#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include "bitmatrix.hpp"
using namespace std;

//  M[row][col]

unsigned int dbj_hash(const string &key);

/* Constructors */

Bitmatrix::Bitmatrix(int rows, int cols)
{
    //error checking if rows or cols is 0 or less
    if (rows <= 0)
    {
        throw((string) "Bad rows");
    }
    if (cols <= 0) 
    {
        throw((string) "Bad cols");
    }

    M.resize(rows);
    for (size_t i = 0; i < M.size(); i++) 
    {
        M[i].resize(cols, '0');
    }
}
           
Bitmatrix::Bitmatrix(const string &fn)
{
    /* Read the bitmatrix from a file.  
    Throw "Can't open file" if you can't open the file.
    Throw "Bad file format" if you can't read the file. */
    ifstream fin;
    string temp = "";
    string tRow = "";
    stringstream ss;
    vector <string> tempV;
    size_t colNum;

    //checks if file opens
    fin.open(fn.c_str());
    if (fin.fail()) throw((string) "Can't open file");

    while (getline(fin, temp) )
    {
        if (!(temp.empty())) {
            ss.str(temp); // may need to loop
        
            while (ss >> temp)
            {
                tRow = tRow + temp;
            }
            tempV.push_back(tRow);
            tRow = "";
            temp = "";
            ss.clear();
        }
    }

    if (tempV.empty()) 
    {
        //cout << "oops" << endl;
        throw((string) "Bad file format");
    }

    //error check if string length is all the same size
    colNum = tempV[0].length();
    for (size_t j = 0; j < tempV.size(); j++) {     
        if (tempV[j].length() != colNum) {
            //cout << "oops" << endl;
            throw((string) "Bad file format");
        }

        for (size_t k = 0; k < colNum; k++)
        {
            if ((tempV[j].at(k) != '0') && (tempV[j].at(k) != '1'))
            {
                //cout << "oops" << endl;
                throw((string) "Bad file format");
            }
        }
    }
        
    fin.close();

    
    M.resize(tempV.size());
    

    M = tempV;
}
        
Bitmatrix *Bitmatrix::Copy() const
{
    //creates a new bitmatrix and copies the M bitmatrix and returns pointer of bm
    Bitmatrix *BM = new Bitmatrix(M.size(), M[0].size()); //need to update this to size of rows and cols 
    BM->M = M; 
    return BM;
}


/* Storage methods */
                 
bool Bitmatrix::Write(const string &fn) const
{
    /* Write to a file.  You will print one line per row of the 
    bitmatrix, and each line is only composed of 0's and 1's.
    Return true if successful and false if not. */
    
    ofstream fin;

    //checks if bitmatrix has been created
    if (M.size() == 0) return false;

    //opening the file and error checking if file was opened properly
    fin.open(fn.c_str());
    if (fin.fail()) {
        fin.close();
        return false;
    }

    for (size_t i = 0; i < M.size(); i++) 
    {
        fin << M.at(i) << endl;
    }

    fin.close();
    return true;
}
 
void Bitmatrix::Print(size_t w) const
{
    //Prints the bit matrix
    size_t numCols;
    size_t numRow = 0;

    numCols = M[0].length();
    for (size_t i = 0; i < M.size(); i++)
    {
        if (numRow == w && w != 0)
        {
            printf("\n");
            numRow = 0;
        }
        for (size_t k = 0; k < numCols; k++)
        {
            cout << M[i].at(k);
            if (w != 0) 
            {
                if (k != numCols-1){
                    if ( ((((k+1) % w) == 0) && (k != 0)) || (w == 1) )
                    {
                    cout << " ";
                    }
                }
            }
        }
        printf("\n");
      
        numRow++;
    }
}
                 
bool Bitmatrix::PGM(const string &fn, int p, int border) const
{
    /* Create a PGM file. Each entry is a p by p square, 
    which is white for zero and 100 for gray.  If border is 
    greater than zero, then there should be a black border
    of that many pixels separating each square and around 
    the whole matrix.  Return true if successful and false 
    if not. */
    
    ofstream fout;
    size_t numCol; //(# of entrys * p) pixels + (# of entrys * border + border) border        COL
    size_t numRow;  //(# of entrys * p) pixels + (# of entrys * border + border) border        ROW            MIght need to -1
    size_t atCol = 0;
    size_t atRow = 0;
    size_t cCount = 0;
    size_t rCount = 0;

    //checks if file is open
    fout.open(fn.c_str());
    if (fout.fail()) return false;

    numCol = (p * M[0].length()) + ((M[0].length() *border) + border);
    numRow = (p * M.size()) + ((M.size() * border) + border);
     
    //writing header to file
    fout << "P2" << endl;
    fout << numCol << " " << numRow << endl;
    fout << "255" << endl;

    for (size_t rowL = 0; rowL < numRow; rowL++)
    {
        for (size_t colL = 0; colL < numCol; colL++)
        {
            if((border != 0) && ((colL % (border+p) < (unsigned) border) || (rowL % (border+p) < (unsigned) border))) // this may need to be changed or adjust the numCol and numRow
            {
                fout << "0   ";
            }
            else 
            {
                if (M[atRow][atCol] == '0')
                {
                    fout << "255 ";
                    cCount++;
                    
                    //Checks if it has printed 255 p number of times
                   if(cCount == (unsigned) p) {
                       atCol++;
                       cCount = 0;
                   }
                } 
                else
                {
                    fout << "100 ";
                    cCount++;

                    if(cCount == (unsigned) p) 
                    {
                       atCol++;
                       cCount = 0;
                   }
                }
                //fout << "100 ";
                
            }
        }
        fout << endl;
        cCount = 0;
        atCol = 0;
        rCount++;
        //checks if row has been printed p number of times
        if ((unsigned)(border + p) == rCount) 
        {
            atRow++;
            rCount = 0;
        }
    }


    fout.close();
  
  return true;
}


/* Access Methods*/

int Bitmatrix::Rows() const
{
    /* Return the number of rows */

    //checking if M has been declared
    if (M.size() == 0) return 1;


    return (int) M.size();
}
                            
int Bitmatrix::Cols() const
{
    /* Returns the number of columns */
    //checking if M has been declared
    if (M.size() == 0) return 1;

    return (int) M[0].size();
}
                            
char Bitmatrix::Val(int row, int col) const
{
    /* Return the specified element ('0' or '1').
    Return 'x' if row or col is bad. */

    //checking if M has been declared
    if (M.size() == 0) return 'x';

    //checks if row is valid
    if (row > (int) M.size()-1) return 'x';

    //checks if col is valid
    if (col > (int) M[0].length()-1) return 'x';

    
    return M[row].at(col);
}


/* Modification Methods */

bool Bitmatrix::Set(int row, int col, char val)
{
    /* Set the specified element to val.
    Val must be 0, 1, '0' or '1'.
    If val is 0 or 1, store '0'/'1' in the matrix.
    Return true if successful and false if not. */
    
    //checking if M has been declared
    if (M.size() == 0) return false ;

    //checks if row is valid
    if (row > (int) M.size()-1) return false;
    if (row < 0) return false;

    //checks if col is valid
    if (col > (int) M[0].length()-1) return false;
    if (col < 0) return false;

    if (val == 1) val = '1';
    if (val == 0) val = '0';

    //checks if val is 0 or 1
    if ((val != '0') && (val != '1')) return false;

    M[row].at(col) = val;
    return true;
}
        
bool Bitmatrix::Swap_Rows(int r1, int r2)
{
    /* Swap these rows.  Return true 
    if successful and false if not. */
    string temp;

    //checking if M has been declared
    if (M.size() == 0) return false;

    //checks if row1 is valid
    if ((r1 < 0) || (r1 > (int) M.size()-1)) return false;

    //checks if row2 is valid
    if ((r2 < 0) || (r2 > (int) M.size()-1)) return false;

    temp = M[r1];
    M[r1] = M[r2];
    M[r2] = temp;
    return true;
}
              
bool Bitmatrix::R1_Plus_Equals_R2(int r1, int r2)
{
    /* Set the row r1 to the sum of row r1 and r2. 
    Return true if successful and false if not. */

    //checking if M has been declared
    if (M.size() == 0) return false;

    //checks if row1 is valid
    if ((r1 < 0) || (r1 > (int) M.size()-1)) return false;

    //checks if row2 is valid
    if ((r2 < 0) || (r2 > (int) M.size()-1)) return false;

    for (size_t i = 0; i < M[r1].size(); i++) {
        
        if (M[r1][i] == M[r2][i])
        {
            M[r1].at(i) = '0';
        }
        else
        {
            M[r1].at(i) = '1';
        }
    }
    return true;
}
      

Bitmatrix *Sum(const Bitmatrix *a1, const Bitmatrix *a2)
{
    //error checking if a1 and a2 have the same size
    if ((a1->Rows() != a2->Rows()) || (a1->Cols() != a2->Cols())) return NULL;
    Bitmatrix *BM = new Bitmatrix (a1->Rows(), a1->Cols());
    int Sum = 0;

    int i = 0;
    int j = 0;

    for (i = 0; i < a1->Rows(); i++) 
    {
        for (j = 0; j < a1->Cols(); j++) 
        {
            Sum = (a1->Val(i,j) ) + (a2->Val(i,j) ); 
            Sum  = Sum % 2; 
            BM->Set(i, j, Sum);
        }
     
//cout << a1->Val(i,j) << " " <<  a2->Val(i,j) << " " << (a1->Val(i,j) + a2->Val(i,j)) % 2 << " ";

        // Sum  = Sum % 2;
        // cout << Sum << endl;
        // BM->Set(i, j, Sum );
    }


    return BM;
}

Bitmatrix *Product(const Bitmatrix *a1, const Bitmatrix *a2)
{
    //error checking to see if a1 cols is == to a2 rows
    if (a1->Cols() != a2->Rows()) return NULL;
    Bitmatrix *BM = new Bitmatrix(a1->Rows(), a2->Cols());
    size_t Sum = 0;

    for (int i = 0; i < a1->Rows(); i++) 
    {
        for (int j = 0; j < a2->Cols(); j++)
        {
            for (int k = 0; k < a1->Cols(); k++)
            {
                Sum = Sum + (a1->Val(i,k) - '0') * (a2->Val(k,j) - '0');
            }
            BM->Set(i, j, (Sum % 2));
            Sum = 0;
        }
    }
    return BM;
}

Bitmatrix *Sub_Matrix(const Bitmatrix *a1, const vector <int> &rows)
{
    if (rows.empty()) return NULL;

    Bitmatrix *BM = new Bitmatrix(rows.size(), a1->Cols());

    for (size_t i = 0; i < rows.size(); i++) 
    {
        for (int k = 0; k < a1->Cols(); k++)
        {
            if (rows[i] > a1->Rows() || rows[i] < 0) return NULL;
            BM->Set(i, k, a1->Val(rows[i], k));
        }
    }
  
    return BM;
}

Bitmatrix *Inverse(const Bitmatrix *m)
{
    if(m->Rows() != m->Cols()) return NULL;

  (void) m;
  return NULL;
}



BM_Hash::BM_Hash(int size)
{
    /* You specify the table size in the constructor. 
    Throw the string "Bad size" if (size <= 0). */
    if (size <= 0) throw((string) "Bad size");
    Table.resize(size);
}

bool BM_Hash::Store(const string &key, Bitmatrix *bm)
{
    /* Store a bitmatrix with the given key. 
    Return true if successful and false if not. 
    Return false if the key is already there. */
    HTE newHTE;
    newHTE.key = key;
    newHTE.bm = bm;
    size_t index;

    index = dbj_hash(key) % Table.size();

    //checking if the key is already in the table
    for (size_t i = 0; i < Table[index].size(); i++) 
    {
        //checking if key is already in the table;
        if (Table[index].at(i).key == newHTE.key) return false;
    }

    Table[index].push_back(newHTE);
    return true;
}
    
Bitmatrix *BM_Hash::Recall(const string &key) const
{
    /* Retrieve a bitmatrix with the given key. 
    Return NULL if unsuccessful. */

    //loops through each table entry looking for the key
    for (size_t i = 0; i < Table.size(); i++) 
    {
        for (size_t k = 0; k < Table[i].size(); k++)
        {
            if (Table[i][k].key == key)
            {
                return Table[i][k].bm;
            }
        }
    }
    return NULL;
}
      
vector <HTE> BM_Hash::All() const
{
    /* Return all of the hash table entries. */
  vector <HTE> rv;
//cout << "fuck" << endl;
    for (size_t i = 0; i < Table.size(); i++) 
    {
        //cout << Table[i][i].bm << endl;
        for (size_t j = 0; j < Table[i].size(); j++)
        {
            //cout << "fuck" << endl;
            // cout << Table[i][i].key << endl;
            if (Table[i][j].key != "")
            {
                rv.push_back(Table[i][j]);
            }
        }
    }

  return rv;
}


//Hash function
unsigned int dbj_hash(const string &key)
{
    size_t i;
    unsigned int h;

    h = 5381;

    for (i = 0; i < key.size(); i ++) 
    {
        h = (h << 5) + h + key[i];
    }
    return h;
}
