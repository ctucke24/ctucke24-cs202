#include <iostream>
#include <string>
#include <vector>
#include <cstdio>
#include <fstream>

using namespace std;


int main () {

string header;
int input; 
int numColumn;
int numRow;
int numPixels; // num pix == numRow * numCol
double avgPix;
vector <int> pixels;
int tempPix;

//getting all the header information and error checks them
cin >> header;
if (header != "P2") {
    cerr << "Bad PGM file -- first word is not P2" << endl;
    return 1;
}

cin >> numColumn;
    if (numColumn <= 0) {
        cerr << "Bad PGM file -- No column specification" << endl;
        return 1;
    }

cin >> numRow;
    if (numRow <= 0) {
        cerr << "Bad PGM file -- No row specification" << endl;
       return 1;
    }

cin >> input; 
    if (input != 255) {
        cerr << "Bad PGM file -- No 255 following the rows and columns" << endl;
        return 1;
    }   

numPixels = numColumn * numRow;

//reads every pixel and if a pixel does not fall in correct value, reports error and ends programn;
for (int i = 0; i < numPixels; i++) {
    cin >> tempPix;
    if ((tempPix < 0) || (tempPix > 255)) {
        cerr << "Bad PGM file -- pixel " << i << " is not a number between 0 and 255" << endl;
        return 1;
    }
    //setting up the average pixel
    avgPix = avgPix + tempPix;

    pixels.push_back(i);
}

//reads to check if there is anything left after all the pixels have been read
if (cin >> input) {
    cerr << "Bad PGM file -- Extra stuff after the pixels" << endl;
    return 1;
}

//divides to get the average pixel value
avgPix = avgPix / numPixels;


//output
printf("# Rows:%12d\n", numRow);
printf("# Columns:%9d\n", numColumn);
printf("# Pixels:%10d\n", numPixels);
printf("Avg Pixel:%9.3f\n", avgPix);



return 0;
}
