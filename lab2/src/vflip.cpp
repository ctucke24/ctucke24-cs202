#include <iostream>
#include <vector>
#include <string>
#include <fstream>
using namespace std;


int main () {

string header;
int input; 
int col;
int row;
int numPixels; // num pix == numRow * numCol
vector <int> pix;
int tempPix;


//reading in header
cin >> header;
if (header != "P2") {
    cerr << "Bad PGM file -- first word is not P2" << endl;
    return 1;
}

cin >> col;
    if (col <= 0) {
        cerr << "Bad PGM file -- No column specification" << endl;
        return 1;
    }

cin >> row;
    if (row <= 0) {
        cerr << "Bad PGM file -- No row specification" << endl;
       return 1;
    }

cin >> input; 
    if (input != 255) {
        cerr << "Bad PGM file -- No 255 following the rows and columns" << endl;
        return 1;
    }   

numPixels = row * col;


//reads pixels and puts them into a vector
for (int i = 0; i < numPixels; i++) {
    cin >> input;
    pix.push_back(input);
}

//reads to check if there is anything left after all the pixels have been read
if (cin >> input) {
    cerr << "Bad PGM file -- Extra stuff after the pixels" << endl;
    return 1;
}


//flips pixels vertically
for (int j = 1; j <= (numPixels / 2); j++) {
    tempPix = pix.at(j);

    pix.at(j) = pix.at(pix.size()-j);

    pix.at(pix.size() - j) = tempPix;
}

//prints PGM header
cout << "P2" << endl;
cout << col << "  " << row << endl;
cout << "255" << endl;

for (int p = 0; p < numPixels; p++) {
    cout << pix.at(p) << " ";

    if(((p%col) == 0) && (p != 0)) {
        cout << endl;
    }
}

//cout << pix.size();

return 0;
}