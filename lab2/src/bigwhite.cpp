#include <iostream>
#include <cstdio>
#include <sstream>
using namespace std;


int main (int argc, char *argv[]) {
    //exits if there is an incorrect arg count
    if (3 != argc) { 
       cerr << "usage: bigwhite rows cols" << endl;
       return 1;
    }
    
    int row, col;
    istringstream str;

    //reading in the args
    str.str((string(argv[1])) + " " + string(argv[2]));

    //checking if str is not a string
    if (str.fail() == true) {
      cerr << "usage: bigwhite rows cols" << endl;
       return 1;
    } 

    //putting str into row and col
    str >> row;
    str >> col;


    //sscanf(argv[1], "%d", &row);
    //sscanf(argv[2], "%d", &col);

    //error checking row and col for invalid numbers
    if (row <= 0) {
      cerr << "usage: bigwhite rows cols" << endl;
      return 1;
    }

    if (col <= 0) {
      cerr << "usage: bigwhite rows cols" << endl;
      return 1;
    }

    //file header information
    cout << "P2" << endl;
    cout << col << " " << row << endl;
    cout << "255" << endl;


    //writing white pixels
    for (int r = 0; r < row; r++) {
      for (int c = 0; c < col; c++) {
        cout << "255 " << endl;
        
    }
    cout << endl;
  }
  return 0;
}