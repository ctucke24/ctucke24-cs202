/*
Clayton Tucker

This program simulates the probabilities of winning a game of keno
*/

#include <iostream>
#include <string>
#include <cstdio>
#include <set>
#include <vector>
#include "fraction.hpp"

using namespace std;

//this program estimates the odds of winning a game of keno

int main () 
{
    double bet;
    int numPick;
    vector <int> catchBalls;            //stores the catch num (aligns with catch payout)
    vector <double> catchPayout;        //stores the payout for the catch (aligns with catch balls)
    vector <double> probablilty;        //stores the probability for each catch (alligns with catch balls)
    vector <double> expectedReturns;    //stores the expected returns for each catch (alligns with catch balls)
    double betReturn = 0;   
    double normReturn = 0;
    double temp;
    Fraction binomBig; //Binom(80-pickNum, 20 - catchBalls)
    Fraction binomPC; //binom(pickNum, catchBalls)
    Fraction binomNum; //binom(80,20)


    //reading in all the inputs
    cin >> bet;
    cin >> numPick;
    while(cin >> temp)              //need to fix
    {
        catchBalls.push_back((int) temp);
        cin >> temp;
        catchPayout.push_back(temp);
    }

    //printing out bets and balls picked
    printf ("Bet: %4.2f\n", bet);
    printf ("Balls Picked: %d\n", numPick);


    //getting probablities and expected returns
    for (size_t i = 0; i < catchBalls.size(); i++)
    {
        binomBig.Multiply_Binom(80-numPick, 20-catchBalls.at(i));
        binomPC.Multiply_Binom(numPick, catchBalls.at(i));
        binomNum.Multiply_Binom(80, 20);
        temp = ((binomBig.Calculate_Product() * binomPC.Calculate_Product()) / binomNum.Calculate_Product());

        probablilty.push_back(temp);
        expectedReturns.push_back(temp * catchPayout[i]);

        //setting up the next catch odds
        binomBig.Clear();
        binomPC.Clear();
        binomNum.Clear();
    }

    //getting return per bet and nomalized return
    for (size_t k = 0; k < catchBalls.size(); k++) {
        betReturn = betReturn + expectedReturns.at(k);
    }
    //sum of expected returns - bet
    betReturn = betReturn - bet;

    //betReturns / bet
    normReturn = betReturn / bet;

    //printing the rest
    for (size_t j = 0; j < catchBalls.size(); j++)
    {
        cout << "  Probability of catching " << catchBalls[j] << " of " << numPick << ": " << probablilty[j] << " -- Expected return: "  << expectedReturns[j] << endl;
    }
    //printing your returns
    printf("Your return per bet: %4.2f\n", betReturn);
    printf("Normalized: %4.2f\n", normReturn);
    return 0;
}