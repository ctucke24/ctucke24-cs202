/*
Clayton Tucker

This program simulates a fraction and does several calculations
*/




#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include "fraction.hpp"

using namespace std;



void Fraction::Clear()  // Clear both the numerator and denominator
{
    numerator.clear();
    denominator.clear();
    return;
}                      

bool Fraction::Multiply_Number(int n)        // Add a number to the numerator
{
    multiset <int>::iterator it;
    //error checking
    if (n < 1) return false;
    if (n == 1) return true; //it doesnt do anything so leave it out

    it = denominator.find(n);
    if((denominator.find(n) != denominator.end()) ) //number is not in the denominator
    {
        denominator.erase(it);
        
        return true; //may need to change
    }
    
    numerator.insert(n);
    return true;
}

bool Fraction::Divide_Number(int n)         // Add a number to the denominator
{
    multiset <int>::iterator it;
    //error checking
    if (n < 1) return false;
    if (n == 1) return true; //it doesnt do anything so leave it out

    it = numerator.find(n);
    if(it != numerator.end()) //check if number is in the denominator
    {
        numerator.erase(it);
        return true; //may need to change
    }
    denominator.insert(n);
    return true;
}

bool Fraction::Multiply_Factorial(int n)    // Add the numbers 2 through n to the numerator
{
    if (n < 1) return false;

    //if (n < 2) return true; //may need to change

    //adds 2 through n to the numerator
    for (int i = 2; i <= n; i++)
    {
        Multiply_Number(i);
    }

    return true;
}

bool Fraction::Divide_Factorial(int n)       // Add the numbers 2 through n to the denominator
{
    if (n < 1) return false;
    
    
    //adds 2 through n to the denominator
    for (int i = 2; i <= n; i++)
    {
        Divide_Number(i);
    }
    return true;
}

bool Fraction::Multiply_Binom(int n, int k)  // Effect multiplying by n-choose-k
{
    //error checking
    if (k > n) return false;
    if (n < 1) return false;
    if (k < 0) return false;

    //n!/ (k! * (n-k)!)
    Multiply_Factorial(n);
    Divide_Factorial(k);
    Divide_Factorial(n-k);
    return true;
}

bool Fraction::Divide_Binom(int n, int k)    // Effect dividing by n-choose-k
{
     //error checking
    if (k > n) return false;
    if (n < 1) return false;
    if (k < 0) return false;

    //(k! * (n-k)!) / n!
    Divide_Factorial(n);
    Multiply_Factorial(k);
    Multiply_Factorial(n-k);
    return true;
}

void Fraction::Invert()                      // Swap the numerator and denominator
{
    multiset <int> temp;
    temp = denominator;
    denominator = numerator;
    numerator = temp;
    return;
}

void Fraction::Print() const                 // Print the equation for the fraction (see the lab writeup)
{
    multiset <int>::iterator index;
  
    //checks if there is a numerator, if not, it just prints 1
    if (0 == numerator.size())
    {
        cout << "1";
    }
    else
    {
        //numerator
        for(index = numerator.begin(); index != numerator.end(); index++)
        {
            if (index != numerator.begin())
            {
                cout << " * ";
            }
            cout << *index;
        }
    }
    
    //if there is no denominator, return
    if (denominator.size() == 0)
    {
        cout << endl;
        return;
    }

    //checks if there is a denominator
    if (denominator.size() > 0)
    {
        cout << " / ";
    }
    else
    {
        return;
    }

    //denominator
    for(index = denominator.begin(); index != denominator.end(); index++)
    {
        if (index != denominator.begin())
        {
            cout << " / ";
        }
        cout << *index;
        
    }
    cout << endl;

    return;
}

double Fraction::Calculate_Product() const   // Calculate the product as a double.
{
    double top = 1;
    double bottom = 1;
    double product;
    multiset <int>::iterator index;
    
    if (numerator.size() == 0) 
    {
        top = 1;
    }
    else
    {
        //adds all the values in the numerator multiset
        for (index = numerator.begin(); index != numerator.end(); index++)
        {
            top = top * *index;
        }
    }

    //checks if there is a denominator, if not just gives the product of the top
    if (denominator.size() == 0)
    {
        return (double) top;
    }

    //adds all the values in the denominator multiset
    for (index = denominator.begin(); index != denominator.end(); index++)
    {
        bottom = bottom * *index;
    }

    product = top / bottom;

    return product;
}