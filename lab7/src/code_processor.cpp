/*
Clayton Tucker
Lab 7: Code Processing

this code simulates a rewards program
*/


#include "code_processor.hpp"
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <map>


using namespace std;

unsigned int djb_hash(const string &s);


Code_Processor::~Code_Processor()
{
    map <string, User *>::iterator nit;
    map <string, Prize *>::iterator pit;

    //goes through every names and prize and deletes the second element
    for (nit = Names.begin(); nit != Names.end(); nit++) delete nit->second;
    for (pit = Prizes.begin(); pit != Prizes.end(); pit++) delete pit->second;
    
    return;
}

//creates a new prize
bool Code_Processor::New_Prize(const std::string &id, const std::string &description, int points, int quantity)
{
    map <string, Prize *>::iterator nit;
    
    //error checking
    if(points <= 0) return false;
    if(quantity <= 0) return false;

    nit = Prizes.find(id);
    if (nit != Prizes.end()) return false;

    Prize *newPrize = new Prize;
    newPrize->id = id;
    newPrize->description = description;
    newPrize->points = points;
    newPrize->quantity = quantity;
    Prizes.insert(make_pair(id, newPrize));
    return true;
}

//creates a new user
bool Code_Processor::New_User(const std::string &username, const std::string &realname, int starting_points)
{
    map <string, User *>::iterator nit;
    
    
    //error checking to use if username is taken
    nit = Names.find(username);
    if(nit != Names.end() ) return false;

    if(starting_points < 0) return false;
    
    User *newUser = new User;
    newUser->username = username;
    newUser->realname = realname;
    newUser->points = starting_points;

    Names.insert(make_pair(username, newUser));
    return true;
}

//deletes a specified user
bool Code_Processor::Delete_User(const std::string &username)
{
    map <string, User *>::iterator nit;
    set <string>::iterator sit;

    nit = Names.find(username);
    if (nit == Names.end()) return false;

    //deleting entries from Phones
    for (sit = nit->second->phone_numbers.begin(); sit != nit->second->phone_numbers.end(); sit++)
    {
        Phones.erase(*sit);
    }

    delete nit->second;
    //deleting User
    Names.erase(username);

    return true;
}

//adds a phone number to a users account
bool Code_Processor::Add_Phone(const std::string &username, const std::string &phone)
{
    map <string, User *>::const_iterator phit;
    map <string, User *>::const_iterator nit;

    //error checking
    nit  = Names.find(username);
    if (nit == Names.end()) return false; //could not find user

    phit = Phones.find(phone);
    if (phit != Phones.end()) return false; //found phone

    //inserts into User class
    nit->second->phone_numbers.insert(phone);

    //inserts into Phones map
    Phones.insert(make_pair(phone, nit->second));

    
    return true;
}

//removes a phone number from a specified account
bool Code_Processor::Remove_Phone(const std::string &username, const std::string &phone)
{
    map <string, User *>::iterator nit;
    map <string, User *>::iterator nit2;
    set <string>::const_iterator pit;

    //error checking
    nit = Names.find(username);
    if (nit == Names.end()) return false;

    nit2 = Phones.find(phone);
    if (nit2 == Phones.end()) return false;
    pit = nit->second->phone_numbers.find(phone); //sets set itterator to location in user phone set.
    if (pit == nit->second->phone_numbers.end()) return false; // may need to change back to !=

    
    nit->second->phone_numbers.erase(pit);  
    Phones.erase(nit2);


    // //erases from Names
    // nit = Names.find(username);
    // nit->second->phone_numbers.erase(pit);

    // nit = Phones.find(phone);
    // Phones.erase(nit);
    return true;
}

//shows all phone numbers on an account
string Code_Processor::Show_Phones(const std::string &username) const
{
    map <string, User *>::const_iterator nit;
    set <string>::iterator phit;
    string pNumbs = "";

    nit = Names.find(username);
    if(nit == Names.end()) return "BAD USER";

    for (phit = nit->second->phone_numbers.begin(); phit != nit->second->phone_numbers.end(); phit++) 
    {
        pNumbs = pNumbs + *phit + "\n";

    }

    return pNumbs;
}

//ender in a code for a chance to redeem points
int Code_Processor::Enter_Code(const std::string &username, const std::string &code)
{
    map <string, User *>::iterator nit;
    set <string>::iterator cit;
    unsigned int hash = 0;

    //error checking
    nit = Names.find(username);
    if(nit == Names.end()) return -1;
    
    cit = Codes.find(code);
    if (cit != Codes.end()) return -1; 

    hash = djb_hash(code);

    
    if (hash % 17 == 0)
    {
        Codes.insert(code);
        nit->second->points = nit->second->points + 10;
        return 10;
    }
    if (hash % 13 == 0)
    {
        Codes.insert(code);
        nit->second->points = nit->second->points + 3;
        return 3;
    }
    return 0;
}

//enter in a code with phone number for a chance to redeem points
int Code_Processor::Text_Code(const std::string &phone, const std::string &code)
{
    map <string, User *>::iterator phit;
    set <string>::iterator cit;
    unsigned int hash = 0;

    //error checking
    phit = Phones.find(phone);
    if(phit == Phones.end()) return -1;
    
    cit = Codes.find(code);
    if (cit != Codes.end()) return -1; 

    hash = djb_hash(code);

    if (hash % 17 == 0)
    {
        Codes.insert(code);
        phit->second->points = phit->second->points + 10;
        return 10;
    }
    if (hash % 13 == 0)
    {
        Codes.insert(code);
        phit->second->points = phit->second->points + 3;
        return 3;
    }

    return 0;
}

//used by the write function 
bool Code_Processor::Mark_Code_Used(const std::string &code)
{
    set <string>::iterator cit;
    unsigned int hash = 0;

    //error checking
    cit = Codes.find(code);
    if(cit != Codes.end()) return false;
    
    hash = djb_hash(code);
    
    //checks if code is valid
    if((hash % 13 == 0) ||(hash % 17 == 0))
    {
        Codes.insert(code);
        return true;
    }

    return false;
}

//prints out user's points
int Code_Processor::Balance(const std::string &username) const
{
    map <string, User *>::const_iterator nit;

    nit = Names.find(username);
    if(nit == Names.end()) return -1; //could not find username in Users

    return nit->second->points; 
}

bool Code_Processor::Redeem_Prize(const std::string &username, const std::string &prize)
{
    map <string, User *>::const_iterator nit;
    map <string, Prize *>::const_iterator pit;

    //error checking
    nit = Names.find(username);
    if(nit == Names.end()) return false;

    pit = Prizes.find(prize);
    if(pit == Prizes.end()) return false;

    // checking if user has less points than prize cost
    if (pit->second->points > Balance(username)) return false;

    //redeem and take one away from quanity
    nit->second->points = nit->second->points - pit->second->points;

    pit->second->quantity--;

    return true;
}
   
//writes output to a file
bool Code_Processor::Write(const std::string &filename) const
{
    ofstream fout; 
    map <string, Prize *>::const_iterator pit;
    map <string, User *>::const_iterator nit;
    set <string>::const_iterator cit;

    //checks if file opens
    fout.open(filename.c_str());
    if (fout.fail()) return false;


    //writes prizes
    for (pit = Prizes.begin(); pit != Prizes.end(); pit++)
    {
        fout << "PRIZE " << pit->first << " " << pit->second->points << " "  << pit->second->quantity << " " << pit->second->description << endl;
    }

    //writes users
    for (nit = Names.begin(); nit != Names.end(); nit++)
    {
        //fout << "pog" << endl;
        fout << "ADD_USER " << nit->first << " " <<  nit->second->points << " " << nit->second->realname <<endl;
    }

    //fout << "not pog" << endl;
    //writes users
    for (nit = Phones.begin(); nit != Phones.end(); nit++)
    {
        fout << "ADD_PHONE " << nit->second->username << " " << nit->first << endl;
    }

    //writes codes
    for (cit = Codes.begin(); cit != Codes.end(); cit++)
    {
        fout << "MARK_USED " << *cit << endl;
    }


    fout.close();
    return true;
}


unsigned int djb_hash(const string &s)
{
  size_t i;
  unsigned int h;

  h = 5381;

  for (i = 0; i < s.size(); i++) {
    h = (h << 5) + h + s[i];
  }
  return h;
}